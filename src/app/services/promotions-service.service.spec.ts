import { TestBed } from '@angular/core/testing';

import { PromotionsServiceService } from './promotions-service.service';

describe('PromotionsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PromotionsServiceService = TestBed.get(PromotionsServiceService);
    expect(service).toBeTruthy();
  });
});
