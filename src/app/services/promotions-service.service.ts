import { Injectable } from '@angular/core';
import {PromotionItem} from '../models/promotion-item';

@Injectable({
  providedIn: 'root'
})
export class PromotionsServiceService {

  public promotionItems: PromotionItem[] = [
    {
      imageSrc: '../../../assets/media/promotions/1.jpg',
      category: 'کارت حافظه',
      actualPrice: 130000,
      promotionPrice: 89000,
      promotionPercent: 32,
      title: 'کارت حافظه‌ microSDXC لکسار مدل High-Performance کلاس 10 استاندارد UHS-I U1 سرعت 45MBps 300X همراه با آداپتور SD ظرفیت 64 گیگابایت',
      description: [
        {
          title: 'ظرفیت',
          detail: '64 گیگابایت'
        },
        {
          title: 'استاندارد سرعت',
          detail: 'Class 10 UHS-I U1'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/2.jpg',
      category: 'خمير دندان',
      actualPrice: 18000,
      promotionPrice: 5900,
      promotionPercent: 67,
      title: 'خمیر دندان پاتریکس مدل Complete Care حجم 130 گرم',
      description: [
        {
          title: 'عصاره',
          detail: 'دارد'
        },
        {
          title: 'سفید کننده',
          detail: 'خیر'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/1.jpg',
      category: 'کارت حافظه',
      actualPrice: 130000,
      promotionPrice: 89000,
      promotionPercent: 32,
      title: 'کارت حافظه‌ microSDXC لکسار مدل High-Performance کلاس 10 استاندارد UHS-I U1 سرعت 45MBps 300X همراه با آداپتور SD ظرفیت 64 گیگابایت',
      description: [
        {
          title: 'ظرفیت',
          detail: '64 گیگابایت'
        },
        {
          title: 'استاندارد سرعت',
          detail: 'Class 10 UHS-I U1'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/2.jpg',
      category: 'خمير دندان',
      actualPrice: 18000,
      promotionPrice: 5900,
      promotionPercent: 67,
      title: 'خمیر دندان پاتریکس مدل Complete Care حجم 130 گرم',
      description: [
        {
          title: 'عصاره',
          detail: 'دارد'
        },
        {
          title: 'سفید کننده',
          detail: 'خیر'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/1.jpg',
      category: 'کارت حافظه',
      actualPrice: 130000,
      promotionPrice: 89000,
      promotionPercent: 32,
      title: 'کارت حافظه‌ microSDXC لکسار مدل High-Performance کلاس 10 استاندارد UHS-I U1 سرعت 45MBps 300X همراه با آداپتور SD ظرفیت 64 گیگابایت',
      description: [
        {
          title: 'ظرفیت',
          detail: '64 گیگابایت'
        },
        {
          title: 'استاندارد سرعت',
          detail: 'Class 10 UHS-I U1'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/2.jpg',
      category: 'خمير دندان',
      actualPrice: 18000,
      promotionPrice: 5900,
      promotionPercent: 67,
      title: 'خمیر دندان پاتریکس مدل Complete Care حجم 130 گرم',
      description: [
        {
          title: 'عصاره',
          detail: 'دارد'
        },
        {
          title: 'سفید کننده',
          detail: 'خیر'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/1.jpg',
      category: 'کارت حافظه',
      actualPrice: 130000,
      promotionPrice: 89000,
      promotionPercent: 32,
      title: 'کارت حافظه‌ microSDXC لکسار مدل High-Performance کلاس 10 استاندارد UHS-I U1 سرعت 45MBps 300X همراه با آداپتور SD ظرفیت 64 گیگابایت',
      description: [
        {
          title: 'ظرفیت',
          detail: '64 گیگابایت'
        },
        {
          title: 'استاندارد سرعت',
          detail: 'Class 10 UHS-I U1'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/2.jpg',
      category: 'خمير دندان',
      actualPrice: 18000,
      promotionPrice: 5900,
      promotionPercent: 67,
      title: 'خمیر دندان پاتریکس مدل Complete Care حجم 130 گرم',
      description: [
        {
          title: 'عصاره',
          detail: 'دارد'
        },
        {
          title: 'سفید کننده',
          detail: 'خیر'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/1.jpg',
      category: 'کارت حافظه',
      actualPrice: 130000,
      promotionPrice: 89000,
      promotionPercent: 32,
      title: 'کارت حافظه‌ microSDXC لکسار مدل High-Performance کلاس 10 استاندارد UHS-I U1 سرعت 45MBps 300X همراه با آداپتور SD ظرفیت 64 گیگابایت',
      description: [
        {
          title: 'ظرفیت',
          detail: '64 گیگابایت'
        },
        {
          title: 'استاندارد سرعت',
          detail: 'Class 10 UHS-I U1'
        }
      ]
    },
    {
      imageSrc: '../../../assets/media/promotions/2.jpg',
      category: 'خمير دندان',
      actualPrice: 18000,
      promotionPrice: 5900,
      promotionPercent: 67,
      title: 'خمیر دندان پاتریکس مدل Complete Care حجم 130 گرم',
      description: [
        {
          title: 'عصاره',
          detail: 'دارد'
        },
        {
          title: 'سفید کننده',
          detail: 'خیر'
        }
      ]
    },
  ];
  public promotionTimeout = '12:53:46';

  constructor() {
  }
}
