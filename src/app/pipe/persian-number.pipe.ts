import { Pipe, PipeTransform } from '@angular/core';
import persianJs from 'persianjs';

@Pipe({
  name: 'persianNumber'
})
export class PersianNumberPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return persianJs(value).englishNumber();
  }

}
