import {PromotionDescription} from './promotion-description';

export interface PromotionItem {
  category: string;
  title: string;
  actualPrice: number;
  promotionPrice: number;
  promotionPercent: number;
  description: PromotionDescription[];
  imageSrc: string;
}
