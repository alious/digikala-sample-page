export interface PromotionDescription {
  title: string;
  detail: string;
}
