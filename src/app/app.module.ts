import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PageComponent } from './components/page/page.component';
import { CardComponent } from './components/card/card.component';
import { SingleFullContainerCarouselComponent } from './components/single-full-container-carousel/single-full-container-carousel.component';
import { PromotionCarouselComponent } from './components/promotion-carousel/promotion-carousel.component';
import { PersianNumberPipe } from './pipe/persian-number.pipe';
import { CustomCurrencyPipe } from './pipe/custom-currency.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    PageComponent,
    CardComponent,
    SingleFullContainerCarouselComponent,
    PromotionCarouselComponent,
    PersianNumberPipe,
    CustomCurrencyPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
