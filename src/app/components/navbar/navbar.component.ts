import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public categories = ['کالای دیجیتال', 'آرایشی، بهداشتی و سلامت', 'خودرو، ابزار و اداری', 'مد و پوشاک', 'خانه و آشپزخانه', 'کتاب، لوازم تحریر و هنر', 'اسباب بازی، کودک و نوزاد', 'ورزش و سفر', 'خوردنی و آشامیدنی'];

  constructor() { }

  ngOnInit() {
  }

}
