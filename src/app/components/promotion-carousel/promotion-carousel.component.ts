import {Component, Input, OnInit} from '@angular/core';
import {PromotionItem} from '../../models/promotion-item';

@Component({
  selector: 'app-promotion-carousel',
  templateUrl: './promotion-carousel.component.html',
  styleUrls: ['./promotion-carousel.component.scss']
})
export class PromotionCarouselComponent implements OnInit {

  @Input() promotionItems: PromotionItem[];
  @Input() promotionTimeout: string;
  private splittedTime: any[];
  public activeItemIndex = 0;

  constructor() { }

  ngOnInit() {
    this.splittedTime = this.promotionTimeout.split(':');
    this.createIntervalForCarousel();
    this.createIntervalForPromotionTimeout();
  }

  public createIntervalForCarousel(): void {
    setInterval(() => {
      this.activeItemIndex === this.promotionItems.length - 1 ? this.activeItemIndex = 0 : this.activeItemIndex ++;
    }, 4000);
  }

  public goToSpecificItem(index): void {
    this.activeItemIndex = index;
  }

  public splittedTimeToNumber(): void {
    this.splittedTime.forEach((item, index, array) => {
      array[index] = parseInt(item);
    });
  }

  public createIntervalForPromotionTimeout(): void {

    setInterval(() => {
      if (this.splittedTime[2] === 0) {
        this.splittedTime[1] --;
        this.splittedTime[2] = 59;
      } else {
        this.splittedTime[2] --;
      }
    }, 1000);
  }

}
