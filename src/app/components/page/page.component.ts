import { Component, OnInit } from '@angular/core';
import {PromotionsServiceService} from '../../services/promotions-service.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  public items: string[];

  constructor(public promotionsService: PromotionsServiceService) { }

  ngOnInit() {
    this.items = [
      '../../../assets/media/carousel/1.jpg',
      '../../../assets/media/carousel/2.jpg',
      '../../../assets/media/carousel/3.jpg',
      '../../../assets/media/carousel/4.jpg',
      '../../../assets/media/carousel/5.jpg',
      '../../../assets/media/carousel/6.jpg',
      '../../../assets/media/carousel/7.jpg'
    ];
  }

}
