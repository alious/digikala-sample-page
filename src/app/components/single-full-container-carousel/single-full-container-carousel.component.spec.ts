import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleFullContainerCarouselComponent } from './single-full-container-carousel.component';

describe('SingleFullContainerCarouselComponent', () => {
  let component: SingleFullContainerCarouselComponent;
  let fixture: ComponentFixture<SingleFullContainerCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleFullContainerCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleFullContainerCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
