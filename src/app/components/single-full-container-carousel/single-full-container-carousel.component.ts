import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-single-full-container-carousel',
  templateUrl: './single-full-container-carousel.component.html',
  styleUrls: ['./single-full-container-carousel.component.scss']
})
export class SingleFullContainerCarouselComponent implements OnInit {
  public activeItemIndex = 0;
  public items: string[];
  @Input() public interval: number;
  @Input() public imagesSrc: string[];

  constructor() { }

  ngOnInit() {
    this.activeItemIndex = 0;
    this.items = this.imagesSrc;
    this.createIntervalForCarousel();
  }

  public createIntervalForCarousel(): void {
    setInterval(() => {
      this.activeItemIndex === 0 ? this.activeItemIndex = this.items.length - 1 : this.activeItemIndex --;
    }, this.interval);
  }

  public goToNextSlide(): void {
    this.activeItemIndex === 0 ? this.activeItemIndex = this.items.length - 1 : this.activeItemIndex --;
  }

  public goToPreviousSlide(): void {
    this.activeItemIndex === this.items.length - 1 ? this.activeItemIndex = 0 : this.activeItemIndex ++;
  }

  public goToSpecificSlide(index): void {
    this.activeItemIndex = index;
  }
}
